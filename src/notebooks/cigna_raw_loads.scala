// Databricks notebook source
// DBTITLE 1,Bootstrap Steps
val region = "us-west-2"
val awsAcctNum = "190254407876"
val env = "staging"  //staging or prod
val roleSessionName = s"${env}-raw-prov-portal"
val roleName = "brighter-cigna-databricks-rds-auth-staging"  // <-- env
val roleARN = s"arn:aws:iam::${awsAcctNum}:role/${roleName}"

val port = 5432
val user = "databricks"
val db = "brighter"
val jdbcHostName = "staging-medical.cluster-c00imrbnxdsl.us-west-2.rds.amazonaws.com"  
//"staging-medical.cluster-c00imrbnxdsl.us-west-2.rds.amazonaws.com"  // <-- env
val schema = "cigna_raw"
val jdbcRdbmsUrl = s"jdbc:postgresql://${jdbcHostName}/${db}?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory"
//val sinkS3Bucket = s"s3a://brighter-cigna-provider-data-incoming-${env}"
val sinkS3Bucket = s"dbfs:/cigna-raw-landing/"

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.rds.auth.GetIamAuthTokenRequest;
import com.amazonaws.services.rds.auth.RdsIamAuthTokenGenerator;
import com.amazonaws.auth.STSAssumeRoleSessionCredentialsProvider;

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.types._

case class HS(tableName:String, fileName:String, delimiter:String, saveMode:SaveMode, schema:StructType)

def getTempAuthToken() : String = {
  // Assume Role to get credentials
  val assumedRoleCreds = new STSAssumeRoleSessionCredentialsProvider.Builder(roleARN, roleSessionName).build

  // Get RDS Auth token
  val sessionToken = RdsIamAuthTokenGenerator.builder()
              .credentials(assumedRoleCreds)  
              .region(region)
              .build();

  // Get the token that will be used as the DB password
  sessionToken.getAuthToken(
              GetIamAuthTokenRequest.builder()
              .hostname(jdbcHostName)
              .port(port)
              .userName(user)
              .build());
}

def readDataWithSchema(s3Bucket:String, delimiter:String, fileName:String, schema: StructType) : DataFrame = {
  val src = s"${s3Bucket}/${fileName}"
  spark.read
    .option("header", true)
    .option("mode","DROPMALFORMED")
    .option("delimiter", delimiter)
    .option("inferSchema", false)
    .schema(schema)
    .csv(src)
}

def jdbcWrite(jdbcDF:DataFrame, dbUrl:String, dbTableSink:String, dbrxUser:String, truncateFlag:Boolean, saveMode:SaveMode) : Unit = {
  val tempAuthToken = getTempAuthToken()
  jdbcDF.write
    .format("jdbc")
    .option("url", dbUrl)
    .option("dbtable", dbTableSink)
    .option("user", dbrxUser)
    .option("password", tempAuthToken)
    .option("truncate", truncateFlag)  //only works when used with 'overwrite' mode
    .option("sessionInitStatement", "SET ROLE TO 'prodenta';") //" DROP TABLE IF EXISTS ${sink}; CREATE TABLE ${sink} ${ddl};")
    .mode(saveMode)  //append, errorifexists, ignore, overwrite (do not use - causes table to be recreated and owned by 'databricks' not 'prodenta')
    .save()  
}

// COMMAND ----------

// DBTITLE 1,CCW:_import_ccw_prim_prov
val csv = HS("_import_ccw_provider_test", "provider_hs_prim_prov_*.tsv", "\t", 
             SaveMode.Overwrite, 
             StructType(Seq(
              StructField("prim_prov_id", StringType, true),
              StructField("prov_id", StringType, true),
              StructField("prov_id_key", StringType, true),
              StructField("frst_nm", StringType, true),
              StructField("mid_nm", StringType, true),
              StructField("last_nm", StringType, true),
              StructField("gendr_cd", StringType, true),
              StructField("birth_year", StringType, true),
              StructField("age", StringType, true),
              StructField("npi", StringType, true),
              StructField("date_created", TimestampType, true))
            ))

val sink = s"${schema}.${csv.tableName}"
var jdbcDF = readDataWithSchema(sinkS3Bucket, csv.delimiter, csv.fileName, csv.schema)
jdbcWrite(jdbcDF, jdbcRdbmsUrl, sink, user, true, csv.saveMode)

// COMMAND ----------

// DBTITLE 1,CCW:_import_ccw_degree
//val csv = HS("_import_degree_test", "provider_hs_prov_degrees_20190206151159.csv", "")
//val csv = HS("_import_ccw_degree_test", "hs-20190311/provider_hs_prov_degrees_*.tsv", "\t")
val csv = HS("_import_ccw_degree_test", "provider_hs_prov_degrees_*.tsv", "\t", 
             SaveMode.Overwrite, 
             StructType(Seq(
              StructField("prov_id", StringType, true),
              StructField("dgree_cd", StringType, true),
              StructField("dgree_desc", StringType, true),
              StructField("date_created", TimestampType, true))
            ))
val sink = s"${schema}.${csv.tableName}"
val jdbcDF = readDataWithSchema(sinkS3Bucket, csv.delimiter, csv.fileName, csv.schema)
jdbcWrite(jdbcDF, jdbcRdbmsUrl, sink, user, true, csv.saveMode)

// COMMAND ----------

// DBTITLE 1,CCW:_import_ccw_education
// val csv = HS("_import_education_test", "provider_hs_prov_education_20190206151159.csv", "")
// val tsv = HS("_import_education_test", "hs-20190311/provider_hs_prov_education_*.tsv", "\t")
val tsv = HS("_import_ccw_education_test", "edu-all.tsv", "\t",
val tsv = HS("_import_ccw_education_test", "provider_hs_prov_education_*.tsv", "\t",
            SaveMode.Overwrite,
            StructType(Seq(
              StructField("prov_id", StringType, true),
              StructField("educatn_insttn_cd", StringType, true),
              StructField("educatn_insttn_nm", StringType, true),
              StructField("gradtn_yr", StringType, true),
              StructField("date_created", TimestampType, true))
            ))
val sink = s"${schema}.${tsv.tableName}"
val jdbcDF = readDataWithSchema(sinkS3Bucket, tsv.delimiter, tsv.fileName, tsv.schema)
jdbcWrite(jdbcDF, jdbcRdbmsUrl, sink, user, true, tsv.saveMode)

// COMMAND ----------

// DBTITLE 1,CCW:_import_ccw_location
//val csv = HS("_import_location_test", "provider_hs_location_20190206151159.csv", "")
// val csv = HS("_import_ccw_location_test", "hs-20190311/provider_hs_location_*.tsv", "\t")
val csv = HS("_import_ccw_location_test", "provider_hs_location_*.tsv", "\t", 
             SaveMode.Overwrite,
             StructType(Seq(
              StructField("prim_prov_id", StringType, true),
              StructField("prov_nm", StringType, true),
              StructField("affiliation_type", StringType, true),
              StructField("addr_ln_1", StringType, true),
              StructField("city_nm", StringType, true),
              StructField("st_cd", StringType, true),
              StructField("postl_cd", StringType, true),
              StructField("date_created", TimestampType, true))
             ))
val sink = s"${schema}.${csv.tableName}"
val jdbcDF = readDataWithSchema(sinkS3Bucket, csv.delimiter, csv.fileName, csv.schema)
jdbcWrite(jdbcDF, jdbcRdbmsUrl, sink, user, true, csv.saveMode)

// COMMAND ----------

// DBTITLE 1,CCW:_import_ccw_license
//val csv = HS("_import_license_test", "provider_hs_prov_licenses_20190206151159.csv", "")
//val csv = HS("_import_ccw_license_test", "hs-20190311/provider_hs_prov_licenses_*.tsv", "\t")
val csv = HS("_import_ccw_license_test", "provider_hs_prov_licenses_*.tsv", "\t", SaveMode.Overwrite, StructType(Seq(
  StructField("prim_prov_id", StringType, true),
  StructField("ref_npi", StringType, true),
  StructField("lic_state_cd", StringType, true),
  StructField("lic_num", StringType, true),
  StructField("date_created", TimestampType, true))
))
val sink = s"${schema}.${csv.tableName}"
val jdbcDF = readDataWithSchema(sinkS3Bucket, csv.delimiter, csv.fileName, csv.schema)
jdbcWrite(jdbcDF, jdbcRdbmsUrl, sink, user, true, csv.saveMode)

// COMMAND ----------

// DBTITLE 1,CCW:_import_ccw_specialty
//val csv = HS("_import_specialty_test", "provider_hs_prov_specialties_20190225163959.csv", "")
//val csv = HS("_import_ccw_specialty_test", "hs-20190311/provider_hs_prov_specialties_*.tsv", "\t")
val csv = HS("_import_ccw_specialty_test", "provider_hs_prov_specialties_*.tsv", "\t", SaveMode.Overwrite, StructType(Seq(
  StructField("prim_prov_id", StringType, true),
  StructField("prov_speclty_cd", StringType, true),
  StructField("prov_speclty_desc", StringType, true),
  StructField("prof_speclty_ctgry_cd", StringType, true),
  StructField("prof_speclty_ctgry_desc", StringType, true),
  StructField("prov_subty_cd", StringType, true),
  StructField("prov_subty_desc", StringType, true),
  StructField("cert_cd", StringType, true),
  StructField("cert_cd_expirn_dt", StringType, true),
  StructField("date_created", TimestampType, true))
))
val sink = s"${schema}.${csv.tableName}"
val jdbcDF = readDataWithSchema(sinkS3Bucket, csv.delimiter, csv.fileName, csv.schema)
jdbcWrite(jdbcDF, jdbcRdbmsUrl, sink, user, true, csv.saveMode)

// COMMAND ----------

// DBTITLE 1,DIR:_import_med_dir_provider
// TODO val csv = HS("_import_prov_raw_test", "xxprov.csv", ",")
val csv = HS("_import_med_dir_provider_test", "provider_hs_provdir_2019*.tsv", "\t", SaveMode.Overwrite, StructType(Seq(
  StructField("prim_prov_id", StringType, true),
  StructField("prov_ty_desc", StringType, true),
  StructField("nm_first", StringType, true),
  StructField("nm_mi", StringType, true),
  StructField("nm_last", StringType, true),
  StructField("nm_sfx", StringType, true),
  StructField("nm_preferred", StringType, true),
  StructField("prov_grp_cd", StringType, true),
  StructField("prov_grp_desc", StringType, true),
  StructField("displ_npi", StringType, true),
  StructField("npi", StringType, true),
  StructField("org_nm", StringType, true),
  StructField("brth_dt", StringType, true),
  StructField("gendr_cd", StringType, true),
  StructField("internship", StringType, true),
  StructField("schools", StringType, true),
  StructField("degrees", StringType, true),
  StructField("languages", StringType, true),
  StructField("collaborations", StringType, true),
  StructField("credentials", StringType, true),
  StructField("date_created", TimestampType, true))
))
val sink = s"${schema}.${csv.tableName}"
val jdbcDF = readDataWithSchema(sinkS3Bucket, csv.delimiter, csv.fileName, csv.schema)
jdbcWrite(jdbcDF, jdbcRdbmsUrl, sink, user, true, csv.saveMode)

// COMMAND ----------

// DBTITLE 1,DIR:_import_med_dir_network
val csv = HS("_import_med_dir_network_test", "provider_hs_provdir_nt_final_*.tsv", "\t", SaveMode.Overwrite, StructType(Seq(
  StructField("prov_id", StringType, true),
  StructField("specialities", StringType, true),
  StructField("mpo_cds", StringType, true),
  StructField("mpo_nms", StringType, true),
  StructField("networks", StringType, true),
  StructField("facilitytypes", StringType, true),
  StructField("hospitaltypes", StringType, true),
  StructField("qualityratings", StringType, true),
  StructField("affiliations", StringType, true),
  StructField("ratings", StringType, true),
  StructField("accreditations", StringType, true),
  StructField("cnsmr_rpt_rtng", StringType, true),
  StructField("coe_ratings", StringType, true),
  StructField("qualityfeature", StringType, true),
  StructField("date_created", TimestampType, true))
))
val sink = s"${schema}.${csv.tableName}"
val jdbcDF = readDataWithSchema(sinkS3Bucket, csv.delimiter, csv.fileName, csv.schema)
jdbcWrite(jdbcDF, jdbcRdbmsUrl, sink, user, true, csv.saveMode)

// COMMAND ----------

// DBTITLE 1,DIR:_import_med_dir_location
val csv = HS("_import_med_dir_location_test", "provider_hs_provdir_nt_loc_final_*.tsv", "\t", SaveMode.Overwrite, StructType(Seq(
  StructField("prov_id", StringType, true),
  StructField("loc_product_cd", StringType, true),
  StructField("loc_contract_start_date", TimestampType, true),
  StructField("loc_contract_end_date", TimestampType, true),
  StructField("loc_specialties", StringType, true),
  StructField("loc_license", StringType, true),
  StructField("pcp_ind", StringType, true),
  StructField("accpt_new_cust_cd", StringType, true),
  StructField("accpt_new_cust_ind", StringType, true),
  StructField("addr_ln1", StringType, true),
  StructField("addr_ln2", StringType, true),
  StructField("city_nm", StringType, true),
  StructField("ste_cd", StringType, true),
  StructField("zip_cd", StringType, true),
  StructField("zip_plus4", StringType, true),
  StructField("cnty_nm", StringType, true),
  StructField("latitude", FloatType, true),
  StructField("longitude", FloatType, true),
  StructField("geo_coding_confidence", IntegerType, true),
  StructField("tel_nums", StringType, true),
  StructField("geo_ind", StringType, true),
  StructField("evng_availty_ind", StringType, true),
  StructField("ofc_handi_access_ind", StringType, true),
  StructField("homeservicesindicator", StringType, true),
  StructField("qmilanguages", StringType, true),
  StructField("offc_hrs", StringType, true),
  StructField("loc_facilitytpes", StringType, true),
  StructField("loc_hospitaltypes", StringType, true),
  StructField("loc_affiliations", StringType, true),
  StructField("pcpdetail", StringType, true),
  StructField("otherlocations", StringType, true),
  StructField("wkend_availty_ind", StringType, true),
  StructField("srvc_ind", StringType, true),  
  StructField("date_created", TimestampType, true))
))
val sink = s"${schema}.${csv.tableName}"
val jdbcDF = readDataWithSchema(sinkS3Bucket, csv.delimiter, csv.fileName, csv.schema)
jdbcWrite(jdbcDF, jdbcRdbmsUrl, sink, user, true, csv.saveMode)

// COMMAND ----------

// DBTITLE 1,DIR:_import_med_dir_network_bkp
// val csv = HS("_import_prov_nt_final_raw_test", s"provider_hs_provdir_nt_final_*.tsv", "\t")
// val sink = s"${schema}.${csv.tableName}"

//   val jdbcDF = spark.read
//     .option("header","true")
//     .option("delimiter", csv.delimiter)
//     .csv(s"s3a://brighter-cigna-provider-data-incoming-${env}/${csv.fileName}")

// val jdbcDF = readDataWithSchema(csv.delimiter, csv.fileName, s)
//display(jdbcDF)
//jdbcWrite(jdbcDF, jdbcRdbmsUrl, sink, user, true, "overwrite")

// val l = List("a", "b", "c", "d", "e", "f", "g", "h", "i", "j")

// l.foreach {x =>
//   val csv = HS("_import_prov_nt_final_raw_test", s"yya${x}.tsv", "")
//   val sink = s"${schema}.${csv.tableName}"
//   val file = csv.fileName
           
//   val jdbcDF = spark.read
//     .option("header","true")
//     .option("delimiter", "\t")
//     .csv(s"s3a://brighter-cigna-provider-data-incoming-staging/${file}")
           
//   if (x == "a") {
//     val tempAuthToken = getTempAuthToken()
//     jdbcDF.write
//       .format("jdbc")
//       .option("url", s"jdbc:postgresql://staging-medical.cluster-c00imrbnxdsl.us-west-2.rds.amazonaws.com/${db}?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory")
//       .option("dbtable", sink)
//       .option("user", user)
//       .option("password", tempAuthToken)
//       .option("truncate", true)  //only works when used with 'overwrite' mode
//       .option("sessionInitStatement", "SET ROLE TO 'prodenta';") //" DROP TABLE IF EXISTS ${sink}; CREATE TABLE ${sink} ${ddl};")
//       .mode("overwrite")  //append, errorifexists, ignore, overwrite (do not use - causes table to be recreated and owned by 'databricks' not 'prodenta')
//       .save()    
//   } else {
//     val tempAuthToken = getTempAuthToken()
//     jdbcDF.write
//       .format("jdbc")
//       .option("url", s"jdbc:postgresql://staging-medical.cluster-c00imrbnxdsl.us-west-2.rds.amazonaws.com/${db}?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory")
//       .option("dbtable", sink)
//       .option("user", user)
//       .option("password", tempAuthToken)
//       .option("truncate", false)  //only works when used with 'overwrite' mode
//       .option("sessionInitStatement", "SET ROLE TO 'prodenta';") //" DROP TABLE IF EXISTS ${sink}; CREATE TABLE ${sink} ${ddl};")
//       .mode("append")  //append, errorifexists, ignore, overwrite (do not use - causes table to be recreated and owned by 'databricks' not 'prodenta')
//       .save()
//   }
// }

// COMMAND ----------

// DBTITLE 1,DIR:_import_med_dir_location_bkp
// val csv = HS("_import_prov_nt_final_raw_test", s"provider_hs_provdir_nt_loc_final_*.tsv", "\t")
// val sink = s"${schema}.${csv.tableName}"

// val s = StructType(Seq(
//   StructField("prov_id", StringType, true),
//   StructField("loc_product_cd", StringType, true),
//   StructField("loc_contract_start_date", StringType, true),
//   StructField("loc_contract_end_date", StringType, true),
//   StructField("loc_specialties", StringType, true),
//   StructField("loc_license", StringType, true),
//   StructField("pcp_ind", StringType, true),
//   StructField("accpt_new_cust_cd", StringType, true),
//   StructField("accot_new_cust_ind", StringType, true),
//   StructField("addr_ln1", StringType, true),
//   StructField("addr_ln2", StringType, true),
//   StructField("city_nm", StringType, true),
//   StructField("ste_cd", StringType, true),
//   StructField("zip_cd", StringType, true),
//   StructField("zip_plus4", StringType, true),
//   StructField("cnty_nm", StringType, true),
//   StructField("latitude", StringType, true),
//   StructField("longitude", StringType, true),
//   StructField("geo_coding_confidence", StringType, true),
//   StructField("tel_nums", StringType, true),
//   StructField("geo_ind", StringType, true),
//   StructField("evng_availty_ind", StringType, true),
//   StructField("ofc_handi_access_ind", StringType, true),
//   StructField("homeservicesindicator", StringType, true),
//   StructField("qmailanguages", StringType, true),
//   StructField("offc_hrs", StringType, true),
//   StructField("loc_facilitytpes", StringType, true),
//   StructField("loc_hospitaltypes", StringType, true), 
//   StructField("loc_affiliations", StringType, true),
//   StructField("pcpdetail", StringType, true),
//   StructField("otherlocations", StringType, true),
//   StructField("wkend_availty_ind", StringType, true),
//   StructField("srvc_ind", StringType, true),
//   StructField("date_created", TimestampType, true))
// )

// val jdbcDF = readDataWithSchema(csv.delimiter, csv.fileName, s)
// display(jdbcDF)
//jdbcWrite(jdbcDF, jdbcRdbmsUrl, sink, user, true, "overwrite")

// //val l = List("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k") 
// val l = List("l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v")

// l.foreach ({ x =>
//   val csv = HS("_import_prov_nt_loc_final_raw_test", s"qtxa${x}.tsv", "")
//   val sink = s"${schema}.${csv.tableName}"
//   val file = csv.fileName
            
//   val jdbcDF = spark.read
//     .option("header","true")
//     .option("delimiter", "\t")
//     .csv(s"s3a://brighter-cigna-provider-data-incoming-staging/${file}")
            
//   if (x == "a") {
//     // JDBC write
//     val tempAuthToken = getTempAuthToken()
//     jdbcDF.write
//       .format("jdbc")
//       .option("url", s"jdbc:postgresql://staging-medical.cluster-c00imrbnxdsl.us-west-2.rds.amazonaws.com/${db}?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory")
//       .option("dbtable", sink)
//       .option("user", user)
//       .option("password", tempAuthToken)
//       .option("truncate", true)  //only works when used with 'overwrite' mode
//       .option("sessionInitStatement", "SET ROLE TO 'prodenta';") //" DROP TABLE IF EXISTS ${sink}; CREATE TABLE ${sink} ${ddl};")
//       .mode("overwrite")  //append, errorifexists, ignore, overwrite (do not use - causes table to be recreated and owned by 'databricks' not 'prodenta')
//       .save()
//   } else {
//     // JDBC write
//     val tempAuthToken = getTempAuthToken()
//     jdbcDF.write
//       .format("jdbc")
//       .option("url", s"jdbc:postgresql://staging-medical.cluster-c00imrbnxdsl.us-west-2.rds.amazonaws.com/${db}?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory")
//       .option("dbtable", sink)
//       .option("user", user)
//       .option("password", tempAuthToken)
//       .option("truncate", false)  //only works when used with 'overwrite' mode
//       .option("sessionInitStatement", "SET ROLE TO 'prodenta';") //" DROP TABLE IF EXISTS ${sink}; CREATE TABLE ${sink} ${ddl};")
//       .mode("append")  //append, errorifexists, ignore, overwrite (do not use - causes table to be recreated and owned by 'databricks' not 'prodenta')
//       .save()
//   }           
// })



// COMMAND ----------

dbutils.notebook.exit("SUCCESS - *SV file extraction to ${jdbcHostName} completed.")
