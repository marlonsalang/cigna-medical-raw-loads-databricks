// Databricks notebook source
// DBTITLE 1,clean up staging areas
import com.brighter.cigna.slack._

SlackMessageService.sendSlackMsgMedical("Medical Reviews", s"Starting: raw_loads_notebooks", SlackMessageType.SUCCESS, MessageCategory.EVENTS)

// COMMAND ----------

// DBTITLE 1,run cigna extraction
dbutils.notebook.run("cigna_extraction", 3600, null)

// COMMAND ----------

// DBTITLE 1,run cigna raw loads
dbutils.notebook.run("cigna_raw_loads", 3600, null)

// COMMAND ----------

// DBTITLE 1,run cleanup
dbutils.notebook.run("cigna_cleanup", 3600, null)

// COMMAND ----------

// DBTITLE 1,return success
SlackMessageService.sendSlackMsgMedical("Medical Reviews", s"Completed: raw_loads_notebooks", SlackMessageType.SUCCESS, MessageCategory.EVENTS)

// COMMAND ----------

// DBTITLE 1,Start/Monitor remaining Stored procs
SlackMessageService.sendSlackMsgMedical("Medical Reviews", s"Starting: raw_loads_stored_proc_monitoring", SlackMessageType.SUCCESS, MessageCategory.EVENTS)

// COMMAND ----------

dbutils.notebook.run("cigna_monitor_raw_load_stored_procs", 3600, null)

// COMMAND ----------

SlackMessageService.sendSlackMsgMedical("Medical Reviews", s"Completed: raw_loads_stored_proc_monitoring", SlackMessageType.SUCCESS, MessageCategory.EVENTS)
