// Databricks notebook source
val d = dbutils.fs.ls("dbfs:/cigna-raw-landing/")
d
  .filter(f1 => !f1.isDir)
  .map({f => dbutils.fs.rm(f.path)})

// COMMAND ----------

val d = dbutils.fs.ls("dbfs:/cigna-raw-keys/")
d
  .filter(f1 => !f1.isDir)
  .map({f => dbutils.fs.rm(f.path)})
