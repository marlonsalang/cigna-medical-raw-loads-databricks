// Databricks notebook source
// DBTITLE 1,Cleanup Staging landing areas...just in case
val d = dbutils.fs.ls("dbfs:/cigna-raw-landing/")
d
  .filter(f1 => !f1.isDir)
  .map({f => dbutils.fs.rm(f.path)})

// COMMAND ----------

dbutils.fs.ls("dbfs:/cigna-raw-landing/")

// COMMAND ----------

// DBTITLE 1,Cleanup keys landing areas...just in case
val d = dbutils.fs.ls("dbfs:/cigna-raw-keys/")
d
  .filter(f1 => !f1.isDir)
  .map({f => dbutils.fs.rm(f.path)})

// COMMAND ----------

dbutils.fs.ls("dbfs:/cigna-raw-keys/")

// COMMAND ----------

// DBTITLE 1,Copy PGP encrypted files from S3 to DBFS
val fileExtension = "zip"
val srcS3Bucket = "s3a://brighter-cigna-provider-data-incoming-staging/"
val sinkLocalData = "dbfs:/cigna-raw-landing/"
val sinkLocalKeys = "dbfs:/cigna-raw-keys/"
val pgpFiles = dbutils.fs.ls(srcS3Bucket).filter(fName => fName.name.endsWith(fileExtension))

if (pgpFiles.isEmpty)
  println("empty list...will go to sleep...")
else {
  println(s"${pgpFiles.length} ${fileExtension} files exist..")
  pgpFiles.foreach{ x =>
    println("--->" + x.name + ", " + x.path)
    dbutils.fs.cp(x.path, sinkLocalData.concat(x.name))
  }
}

// COMMAND ----------

// DBTITLE 1,PGP import 
dbutils.fs.cp(srcS3Bucket.concat("pgp_cigna_providers_sandbox_private.asc"), sinkLocalKeys.concat("prv.asc"))
dbutils.fs.cp(srcS3Bucket.concat("pgp_cigna_providers_sandbox_private_password.txt"), sinkLocalKeys.concat("pss.txt"))
dbutils.fs.cp(srcS3Bucket.concat("pgp_cigna_providers_sandbox_public.asc"), sinkLocalKeys.concat("pub.asc"))

// COMMAND ----------

// DBTITLE 1,See PGP items
// MAGIC %sh
// MAGIC ls /dbfs/cigna-raw-keys

// COMMAND ----------

// DBTITLE 1,Show keys
// MAGIC %sh
// MAGIC gpg --list-keys

// COMMAND ----------

// DBTITLE 1,import priv key and decrypt
// MAGIC %sh
// MAGIC gpg --import /dbfs/cigna-raw-keys/prv.asc
// MAGIC gpg --import /dbfs/cigna-raw-keys/pub.asc

// COMMAND ----------

// DBTITLE 1,gpg decrypt
// MAGIC %sh
// MAGIC cd /dbfs/cigna-raw-landing/
// MAGIC cat /dbfs/cigna-raw-keys/pss.txt | gpg --batch --passphrase-fd 0 --output /dbfs/cigna-raw-landing/file.zip --decrypt /dbfs/cigna-raw-landing/provider_headshots_*.zip.pgp

// COMMAND ----------

// MAGIC %sh
// MAGIC cd /dbfs/cigna-raw-landing/
// MAGIC ls

// COMMAND ----------

// DBTITLE 1,unzip file
// MAGIC %sh
// MAGIC cd /dbfs/cigna-raw-landing/
// MAGIC unzip file.zip

// COMMAND ----------

dbutils.fs.ls("dbfs:/cigna-raw-landing")

// COMMAND ----------

// DBTITLE 1,return success
dbutils.notebook.exit("SUCCESS - PGP decryption/UNZIP extraction completed.")
